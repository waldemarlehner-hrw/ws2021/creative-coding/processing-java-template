package de.enterYourDomainHere.processingTemplate;

import processing.core.PApplet;

public class Main extends PApplet
{
    /// Settings
    private final static float radius = 100f;
    private final static float speed = 0.1f;
    private final static int screenSize = 500;
    ///

    private static int frameCounter = 0;

    private static Pair<Double, Double> getNewPointPosition()
    {
        int position = frameCounter++;
        double x = Math.sin( -position * speed );
        double y = Math.cos( position * speed );
        x *= radius;
        y *= radius;
        x += ((float) screenSize) / 2;
        y += ((float) screenSize) / 2;

        return new Pair<>( x, y );
    }


    @Override
    public void settings()
    {
        size( screenSize, screenSize );
    }

    @Override
    public void draw()
    {
        background( 0xB0B0B0 );
        Pair<Double, Double> newPosition = getNewPointPosition();
        ellipse( newPosition.getFirst().floatValue(), newPosition.getSecond().floatValue(), 10, 10 );
    }

    // Program entry point
    public static void main( String[] args )
    {
        PApplet.main( Main.class, args );
    }

}
